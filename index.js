let buffer="0";
let upgradeData=0;
let tempSign;
const screen=document.getElementById("input_data");

function buttonClick(value){
    if(value=="c"){
        buffer="0";
    }
    else if(value=="←"){
        if(buffer.length === 1){
            buffer="0";
        }
        else{
            buffer=buffer.substring(0,buffer.length-1);
        }
    }
    else if (isNaN(value)){
        handleSign(value);
    }
    else{
        handleData(value);
    }
    screenData();

}

function handleData(value){
    if(buffer === "0"){
      buffer = value;
    }
    else{
        buffer+=value;
    }
}
function calculateData(value){
    if(buffer === "0"){
        return;
    }
    const temp = parseInt(buffer);
    if(upgradeData===0){
        upgradeData=temp;
    }
    else{
        computeData(temp);
    }

    tempSign=value;
    buffer="0";
}
let newData;
function computeData(newData){
    if(tempSign === "+"){
        upgradeData += +newData;
    }
    else if(tempSign === "-"){
        upgradeData -= newData;
    }
    else if(tempSign === "×"){
        upgradeData *= newData;
    }
    else {
        upgradeData /= newData;
    }
}
    


function handleSign(value){
    switch(value){
        case "+": calculateData(value);
                    break;
        case "-": calculateData(value);
                    break;
        case "×": calculateData(value);
                    break;
        case "÷": calculateData(value);
                    break;
        case "=": if(tempSign===null){
                    return;
                }
                computeData(buffer);
                tempSign=null;
                buffer= +upgradeData;
                upgradeData=0;
                break;

    }
}

function screenData(){
    screen.innerText=buffer;
}


const divs = document.querySelectorAll('.button_properties');
divs.forEach(iterator => iterator.addEventListener('click', event => {
  buttonClick(event.target.innerText);
  console.log(event.target.innerText);
}));
