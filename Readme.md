# Calculator Project

## In this project, I have made a calculator project by using:
* HTML
* CSS
* JavaScript
## Project Description
* In this project, I have made a calculator project using HTML, CSS and JavaScript with a beautiful layout and user interface.
* You can use this calculator for your financial use in your daily life
## Visit my site to use the calculator.
* https://main--peaceful-gumdrop-2589f8.netlify.app/

